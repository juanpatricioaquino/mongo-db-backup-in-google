# Utiliza una imagen de Node.js
FROM node:14

# Establece el directorio de trabajo en el contenedor
WORKDIR /app

# Copia el script de respaldo y las credenciales de Google Drive al contenedor
COPY backup-script.js /app
COPY ServiceAccountCredentials.json /app


# Instala la utilidad mongodump
ADD https://fastdl.mongodb.org/tools/db/mongodb-database-tools-ubuntu1804-x86_64-100.5.0.deb /mongodb-tools.deb
RUN apt install /mongodb-tools.deb
RUN rm /mongodb-tools.deb

# Instala la utilidad zip
RUN apt-get update && apt-get install -y zip

# Instala las dependencias necesarias
RUN npm install googleapis mongodb node-cron dotenv

# Comando para ejecutar el script
CMD ["node", "backup-script.js"]