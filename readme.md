# MongoDB Backup and Upload to Google Drive

Este script en JavaScript está diseñado para realizar copias de seguridad de una base de datos MongoDB y luego cargar la copia de seguridad resultante en Google Drive. Utiliza Node.js y diversas bibliotecas, incluidas `node-cron` y `googleapis`, para automatizar el proceso de respaldo y carga.

## Requisitos

Asegúrate de que cumplas con los siguientes requisitos antes de utilizar este script:

1. Node.js: Asegúrate de tener Node.js instalado en tu sistema.

2. Credenciales de Google Drive: Debes tener credenciales válidas de Google Drive en formato JSON, que se deben guardar en un archivo llamado `ServiceAccountCredentials.json` en el mismo directorio que el script.

3. Variables de entorno: Configura las siguientes variables de entorno en un archivo `.env`:

MONGO_URI=URI de tu base de datos MongoDB
OS=Linux o Windows_NT
CRON_MONGODB_BACKUP=0 0 * * * (expresión cron para la programación del respaldo diario)
GOOGLE_DRIVE_PARENT_FOLDER_ID=google_folder_id

4. Paquetes de Node.js: Asegúrate de instalar las dependencias necesarias ejecutando `npm install`.

## Uso

Para ejecutar el script, simplemente ejecuta el siguiente comando:

node backup-script.js

El script utilizará la configuración de respaldo programado definida en `CRON_MONGODB_BACKUP` para realizar respaldos diarios de tu base de datos MongoDB y cargarlos en Google Drive.

## Opciones de Compresión

El script admite dos opciones de compresión para los archivos de respaldo:

- Linux: Utiliza `zip` para comprimir los archivos en un archivo ZIP.
- Windows: Utiliza `7z` para crear un archivo ZIP.

Puedes configurar la opción correcta según tu sistema operativo definiendo la variable `OS` en las variables de entorno.

## Autor

Juan Aquino

---
**Nota**: Asegúrate de mantener seguras tus credenciales y variables de entorno. No las compartas públicamente en tu repositorio.

