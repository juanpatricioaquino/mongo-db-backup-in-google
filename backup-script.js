const cron = require("node-cron");
const { exec } = require("child_process");
const fs = require("fs");
const { google } = require("googleapis");

require("dotenv").config();

// Lista de variables de entorno que para verificar
const envVariablesToCheck = [
  "OS",
  "MONGO_URI",
  "GOOGLE_DRIVE_PARENT_FOLDER_ID",
  "CRON_MONGODB_BACKUP",
];

// Itera sobre las variables de entorno y verifica si alguna está vacía
for (const envVariable of envVariablesToCheck) {
  checkEnvVariable(envVariable);
}

// Configura las credenciales de Google Drive
const credentials = require("./ServiceAccountCredentials.json");
const SCOPES = ["https://www.googleapis.com/auth/drive.file"];
const auth = new google.auth.GoogleAuth({
  credentials,
  scopes: SCOPES,
});

// Directorio temporal para almacenar los archivos de respaldo
const PATH_DATABASE_FILES = "./dump/gestion-stock";
const PATH_DUMP_FILES = "./dump";

// OS utilizado para seleccionar el comando para zippear archivos .bson
const OS = process.env.OS;

// Ejecuta 'mongodump' para crear una copia de seguridad en un directorio temporal
const generateMongoDump = async function () {
  return new Promise(async (resolve, reject) => {
    try {
      if (fs.existsSync(PATH_DUMP_FILES)) {
        // Si existe, borra la carpeta y su contenido de manera recursiva
        await exec(`rm -r ${PATH_DUMP_FILES}`);
      }
      exec(`mongodump --uri=${process.env.MONGO_URI}`, (err) => {
        if (err) {
          reject("Error al generar respaldo de MongoDB completado: " + err);
        } else {
          console.info("Respaldo de MongoDB completado.");
          resolve();
        }
      });
    } catch (err) {
      reject("Error al generar respaldo de MongoDB completado: " + err);
    }
  });
};

const zipMongoDump = async function () {
  return new Promise((resolve, reject) => {
    const TODAY = new Date();
    const FILENAME = `${TODAY.getFullYear()}-${TODAY.getMonth() + 1}-${TODAY.getDate()}.zip`;
    const PATH_OUTPUT_ZIP = `./${FILENAME}`;
    let command;
    switch (OS) {
      case "LINUX":
        command = `zip -r ${PATH_OUTPUT_ZIP} ${PATH_DATABASE_FILES} `;
        break;

      case "Windows_NT":
        command = `7z a -tzip ${PATH_OUTPUT_ZIP} ${PATH_DATABASE_FILES}`;
        break;

      default:
        return;
    }
    try {
      exec(command, (err) => {
        if (err) {
          reject(`Error al generar ${FILENAME}: ` + err);
        } else {
          console.info(`Archivo ${FILENAME} generado`);
          resolve();
        }
      });
    } catch (err) {
      reject(`Error al generar ${FILENAME}: ` + err);
    }
  });
};

const uploadMongoBackup = async function (auth) {
  return new Promise((resolve, reject) => {
    try {
      console.log("Archivos BSON comprimidos en el archivo ZIP.");
      // Sube el archivo ZIP a Google Drive
      const drive = google.drive({ version: "v3", auth });

      const fileMetadata = {
        name: FILENAME,
        parents: ["13_Q1YpjqUnSeONvWoJVl8NcspMzhqdJa"],
      };
      const media = {
        mimeType: "application/zip",
        body: fs.createReadStream(PATH_OUTPUT_ZIP),
      };

      drive.files.create(
        {
          resource: fileMetadata,
          media: media,
          fields: "id",
        },
        (err, file) => {
          if (err) {
            console.error("Error al cargar el archivo a Google Drive:", err);
            reject("Error al cargar el archivo a Google Drive: " + err);
          } else {
            console.log(
              "Respaldo completado y cargado a Google Drive con el ID:",
              file.data.id
            );

            // Borra el archivo ZIP temporal
            fs.unlinkSync(PATH_OUTPUT_ZIP);

            resolve();
          }
        }
      );
    } catch (err) {
      reject("Error al subir el archivo a Google Drive: " + err);
    }
  });
};

cron.schedule(
  process.env.CRON_MONGODB_BACKUP,
  async () => {
    console.log("CronManager empezó");
    try {
      generateMongoDump().then(() => {
        zipMongoDump().then(() => {
          uploadMongoBackup(auth).then(() => {
            console.log("CronManager termino");
          });
        });
      });
    } catch (e) {
      console.error("CronManager error", e);
    }
  },
  { scheduled: true }
);

function checkEnvVariable(variableName) {
  const value = process.env[variableName];
  if (!value) {
    throw new Error(
      `La variable de entorno '${variableName}' está vacía o no definida.`
    );
  }
}
